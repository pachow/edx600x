#!/usr/bin/env python2
# -*- coding: utf-8
#
#* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
# File Name : balance.py
# Creation Date : 22-02-2013
# Created By : Axilleas Pipinellis <axilleas@archlinux.gr>
# For : Edx 600x course
# Week : 2
# Pset : 2
#_._._._._._._._._._._._._._._._._._._._._._._._._._._._.*/
#



def payment(balance, annualInterestRate, monthlyPaymentRate):
    '''numbers -> number

    Calculates the credit card balance after one year if a person only pays the
    minimum monthly payment required by the credit card company each month.

    payment(4213, 0.2, 0.04)
    >>> Total Paid: 1775.55
        Remaining balance: 3147.67
    
    payment(4842, 0.2, 0.4)
    >>> Total paid: 2040.64
        Remaining balance: 3617.62
    '''

    totalPaid = 0

    for month in range(12):
        payment = balance  * monthlyPaymentRate
        balance = (balance - payment) * (1 + annualInterestRate/12)
        totalPaid += payment
#        print('Month: ' + str(month+1))
#        print('Minimum monthly payment: ' + str(round(payment,2)))
#        print('Remaining balance: ' + str(round(balance,2)))
    
    return 'Total paid: ', round(totalPaid,2)
    return 'Remaining balance: ', round(balance,2)


def fixed_payment(balance, annualInterestRate):
    '''numbers -> int

    Returns the smallest monthly payment such that we can pay off
    the entire balance within a year.

    fixed_payment(3329, 0.2)
    >>> Lowest Payment: 310
    
    fixed_payment(389, 0.25)
    >>> Lowest Payment: 40
    '''

    minPayment = 0
    newbalance = 0
    initBalance = balance # keep track of the initial balance

    while balance >= 0:
        minPayment += 10
        balance = initBalance
        for month in range(12): # total balance at the end of year
	         balance = (balance - minPayment) * (1 + annualInterestRate/12)

    return 'Lowest Payment: ', minPayment

def bisect_payment(balance, annualInterestRate):
    '''numbers -> number

    Returns the smallest monthly payment such that we can pay off
    the entire balance within a year, using bisection search.

    bisect_payment(3329, 0.2)
    >>> Lowest Payment: 310
    
    bisect_payment(389, 0.25)
    >>> Lowest Payment: 40
    '''

    low = balance / 12.0 # Monthly payment lower bound
    high = (balance * (1 + annualInterestRate)**12) / 12.0 # Monthly payment upper bound 
    tol = 0.01
    nmax = 1000
    n = 1
    initBalance = balance

    while n <= nmax:
    
        minPayment = (low + high)/2
        
        for month in range(12):
            balance = (balance - minPayment)*(1 + annualInterestRate/12)

        if abs(balance) <= tol:
            return round(minPayment,2)

        n += 1

        if balance > 0:
             low = minPayment
        else:
            high = minPayment
        
        balance = initBalance

    print('Maximum steps exceeded. No solution found')

    
    
    
    
    
    
    
    
    
